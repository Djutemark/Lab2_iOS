//
//  JobsViewController.swift
//  Lab2_iOS
//
//  Created by Dennis Jutemark on 2017-11-18.
//  Copyright © 2017 Dennis Jutemark. All rights reserved.
//

import UIKit

class JobsViewController: UIViewController {

    @IBOutlet weak var Header: UILabel!
    @IBOutlet weak var ExperienceDescription: UITextView!
    @IBOutlet weak var JobImage: UIImageView!
    
    var exp: WorkEpxerience = WorkEpxerience(name: "nil", description: "null", imageName: "null")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Header.text = exp.name
        ExperienceDescription.text = exp.description
        JobImage.image = UIImage(named: exp.imageName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
