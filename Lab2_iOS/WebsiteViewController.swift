//
//  WebsiteViewController.swift
//  Lab2_iOS
//
//  Created by Dennis Jutemark on 2017-11-18.
//  Copyright © 2017 Dennis Jutemark. All rights reserved.
//

import UIKit
import WebKit

class WebsiteViewController: UIViewController, WKUIDelegate {
    var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://en.wiktionary.org/wiki/GIYF")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
