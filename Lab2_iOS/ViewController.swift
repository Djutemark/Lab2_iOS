//
//  ViewController.swift
//  Lab2_iOS
//
//  Created by Dennis Jutemark on 2017-10-30.
//  Copyright © 2017 Dennis Jutemark. All rights reserved.
//

import UIKit

class Cell1: UITableViewCell {
    @IBOutlet weak var potraitImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
}

class Cell2: UITableViewCell {
    @IBOutlet weak var phoneLabel: UILabel!
}

class Cell3: UITableViewCell {
    @IBOutlet weak var emailLabel: UILabel!
}

class Cell4: UITableViewCell {
    @IBOutlet weak var mapLabel: UILabel!
}

class Cell5: UITableViewCell {
    @IBOutlet weak var websiteLabel: UILabel!
}

class WorkExpCell: UITableViewCell {
    @IBOutlet weak var workExpLabel: UILabel!
}

class WorkEpxerience {
    let name, description, imageName: String
    
    init(name: String, description: String, imageName: String) {
        self.name = name
        self.description = description
        self.imageName = imageName
    }
}

class DemoCell: UITableViewCell {
    @IBOutlet weak var demoLabel: UILabel!
}

class DemoData {
    let name: String
    
    init (name: String) {
        self.name = name
    }
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let workExperiences = [
        WorkEpxerience(name: "JU", description: "En utbildning på Jönköping University", imageName: "JU.jpg"),
        WorkEpxerience(name: "CDAB", description: "Anställd på CDAB sedan 2015", imageName: "CDAB.jpg")
    ]
    var workExperiencesLoaded = 0
    
    let demos = [
        DemoData(name: "A cool Demo!")
    ]
    var demosLoaded = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toJobs") {
            let controller = segue.destination as! JobsViewController
            let workExpName = (sender as? WorkExpCell)?.workExpLabel.text
            
            for exp in workExperiences {
                if exp.name == workExpName {
                    controller.exp = exp
                    break
                }
            }
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Going to get sections! + ", section)
        switch section {
        case 0:
            return 5
        case 1:
            return workExperiences.count
        case 2:
            return demos.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:
            return "Work Experience"
        case 2:
            return "Demos"
        default:
            return ""
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("Going to populate...")
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                return tableView.dequeueReusableCell(withIdentifier: "cell1") as! Cell1
            case 1:
                return tableView.dequeueReusableCell(withIdentifier: "cell2") as! Cell2
            case 2:
                return tableView.dequeueReusableCell(withIdentifier: "cell3") as! Cell3
            case 3:
                return tableView.dequeueReusableCell(withIdentifier: "cell4") as! Cell4
            case 4:
                return tableView.dequeueReusableCell(withIdentifier: "cell5") as! Cell5
            default:
                print("Lets return workExpCell, shouldn't be able to get here at all.....")
                return tableView.dequeueReusableCell(withIdentifier: "workExpCell") as! WorkExpCell
            }
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "workExpCell") as! WorkExpCell
            cell.workExpLabel.text = workExperiences[workExperiencesLoaded].name
            workExperiencesLoaded += 1
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "demoCell") as! DemoCell
            cell.demoLabel.text = demos[demosLoaded].name
            demosLoaded += 1
            return cell
        }
    }
}

