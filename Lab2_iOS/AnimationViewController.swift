//
//  DemoViewController.swift
//  Lab2_iOS
//
//  Created by Dennis Jutemark on 2017-11-18.
//  Copyright © 2017 Dennis Jutemark. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {

    @IBOutlet weak var AnimateButton: UIButton!
    @IBOutlet weak var Image: UIImageView!
    
    var points = [
        CGPoint(x: 125, y: 250),
        CGPoint(x: 200, y: 325),
        CGPoint(x: 275, y: 250)
    ]
    
    var state = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.Image.center = points[state]
    }

    @IBAction func AnimateButtonTouchDown(_ sender: Any) {
        UIView.animate(withDuration: 5,
            animations: {
                self.state = (self.state + 1) % self.points.count
                self.Image.center = self.points[self.state]
            }
        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
